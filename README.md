# OS_Experiment

## Build instructions

```sh
rustup install nightly;
rustup component add rust-src;
rustup component add llvm-tools-preview;
cargo install cargo-xbuild;
cargo install bootimage --version "^0.7.7";
```