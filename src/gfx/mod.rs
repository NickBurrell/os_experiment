#[cfg(all(
    feature = "bios",
    target_os = "none",
    any(target_arch = "x86", target_arch = "x86_64")
))]
pub mod bios;
#[cfg(all(
    feature = "bios",
    target_os = "none",
    any(target_arch = "x86", target_arch = "x86_64")
))]
pub use bios::*;

#[cfg(all(
    feature = "uefi",
    target_os = "none",
    any(target_arch = "x86", target_arch = "x86_64")
))]
pub mod uefi;
#[cfg(all(
    feature = "uefi",
    target_os = "none",
    any(target_arch = "x86", target_arch = "x86_64")
))]
pub use uefi::*;

#[cfg(all(
    any(target_arch = "x86", target_arch = "x86_64"),
    not(any(feature = "bios", feature = "uefi"))
))]
core::compile_error!("Feature not defined");
