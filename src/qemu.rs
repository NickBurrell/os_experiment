#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u32)]
pub enum QemuExitCode {
    Success = 0x10,
    Failed = 0x11,
}

pub fn exit(exit_code: QemuExitCode) {
    use platform::arch::x86_64::io::Port;
    unsafe {
        Port::new(0xf4).write(exit_code as u32);
    }
}
