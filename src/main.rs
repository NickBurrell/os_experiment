#![no_std]
#![no_main]
#![feature(asm)]
#![feature(const_fn)]
#![cfg(any(target_arch = "x86", target_arch = "x86_64"))]
#![feature(abi_x86_interrupt)]
#![feature(custom_test_frameworks)]
#![test_runner(os_experiment::test::test_runner)]
#![reexport_test_harness_main = "test_main"]

extern crate alloc;

use bootloader::{entry_point, BootInfo};
use core::panic::PanicInfo;
use os_experiment::memory::allocator;
use os_experiment::{print, println};

#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    println!("{}", info);
    os_experiment::hlt_loop();
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    os_experiment::kernel::panic::test_panic_handler(info)
}

entry_point!(kernel_main);

#[no_mangle]
pub fn kernel_main(boot_info: &'static BootInfo) -> ! {
    os_experiment::init();

    let phys_mem_offset = platform::addr::VirtualAddress::new(boot_info.physical_memory_offset);
    let mut mapper = unsafe { os_experiment::memory::init(phys_mem_offset) };
    let mut frame_allocator =
        unsafe { os_experiment::memory::BootInfoFrameAllocator::init(&boot_info.memory_map) };

    #[cfg(test)]
    test_main();

    allocator::init_heap(&mut mapper, &mut frame_allocator).expect("heap initialization failed");

    os_experiment::hlt_loop();
}
