#![no_std]
#![cfg_attr(test, no_main)]
#![feature(custom_test_frameworks)]
#![feature(asm)]
#![feature(const_fn)]
#![cfg(any(target_arch = "x86", target_arch = "x86_64"))]
#![feature(abi_x86_interrupt)]
#![feature(alloc_error_handler)]
#![test_runner(crate::test::test_runner)]
#![reexport_test_harness_main = "test_main"]

extern crate alloc;

#[macro_use]
pub mod vga;

#[macro_use]
pub mod gfx;

#[macro_use]
pub mod kernel;
pub mod memory;

pub mod qemu;
pub mod test;

pub use kernel::serial;

pub use alloc::{boxed::Box, rc::Rc, vec, vec::Vec};

#[cfg(test)]
use bootloader::{entry_point, BootInfo};

use linked_list_allocator::LockedHeap;

#[global_allocator]
static ALLOCATOR: LockedHeap = LockedHeap::empty();

#[cfg(test)]
entry_point!(test_kernel_main);

#[cfg(test)]
#[no_mangle]
pub fn test_kernel_main(_boot_info: &'static BootInfo) -> ! {
    init();
    test_main();
    hlt_loop();
}

pub fn hlt_loop() -> ! {
    loop {
        unsafe { asm!("hlt" :::: "volatile") };
    }
}

pub fn init() {
    kernel::gdt::init();
    kernel::interrupts::init_idt();
    unsafe {
        kernel::interrupts::PICS.lock().initialize();
        asm!("sti" :::: "volatile");
    };
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &core::panic::PanicInfo) -> ! {
    kernel::panic::test_panic_handler(info)
}

#[alloc_error_handler]
fn alloc_error_handler(layout: alloc::alloc::Layout) -> ! {
    panic!("allocation error: {:?}", layout)
}
