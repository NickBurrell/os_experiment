use crate::qemu::{exit, QemuExitCode};

#[doc(hidden)]
pub fn test_runner(tests: &[&dyn Fn()]) {
    serial_println!("Running {} tests", tests.len());
    for test in tests {
        test();
    }
    exit(QemuExitCode::Success)
}
