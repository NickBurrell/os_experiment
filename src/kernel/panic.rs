use core::panic::PanicInfo;

pub fn test_panic_handler(info: &PanicInfo) -> ! {
    serial_println!("[failed]\n");
    serial_println!("Error: {}\n", info);
    crate::qemu::exit(crate::qemu::QemuExitCode::Failed);
    crate::hlt_loop();
}
