#[rustfmt::skip]
enum FisType {
    FIS_TYPE_REG_H2D   = 0x27,
    FIS_TYPE_REG_D2H   = 0x34,
    FIS_TYPE_REG_ACT   = 0x41,
    FIS_TYPE_REG_SETUP = 0x46,
    FIS_TYPE_DATA      = 0x58,
    FIS_TYPE_PIO_SETUP = 0x5F,
    FIS_TYPE_DEV_BITS  = 0xA1,
}
