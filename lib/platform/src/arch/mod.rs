#[cfg(target_arch = "aarch64")]
pub mod aarch64;
#[cfg(target_arch = "arm")]
pub mod arm;
#[cfg(target_arch = "x86")]
pub mod x86;
#[cfg(target_arch = "x86_64")]
pub mod x86_64;

#[cfg(any(target_arch = "x86", target_arch = "x86_64"))]
pub mod x86_common;

pub mod common;
pub use common::io;
