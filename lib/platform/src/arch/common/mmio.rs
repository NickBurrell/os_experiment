pub mod io {
    pub mod mmio {
        #[cfg(target_pointer_width = "32")]
        type AddressType = u32;
        #[cfg(target_pointer_width = "64")]
        type AddressType = u64;

        pub trait MemoryMappedReader {
            unsafe fn read_address(addr: AddressType) -> Self;
        }

        pub trait MemoryMappedWriter {
            unsafe fn write_address(addr: AddressType, value: Self);
        }
        pub trait MemoryMappedReaderWriter: MemoryMappedReader + MemoryMappedWriter {}
        macro_rules! impl_mmio_reader {
            ($name:ident) => {
                impl MemoryMappedReader for $name {
                    #[inline]
                    unsafe fn read_address(addr: AddressType) -> Self {
                        (addr as *mut Self).read_volatile()
                    }
                }
            };
        }
        macro_rules! impl_mmio_writer {
            ($name:ident) => {
                impl MemoryMappedWriter for $name {
                    #[inline]
                    unsafe fn write_address(addr: AddressType, value: Self) {
                        (addr as *mut Self).write_volatile(value);
                    }
                }
            };
        }

        macro_rules! impl_mmio_reader_writer {
            ($name:ident) => {
                impl_mmio_reader!($name);
                impl_mmio_writer!($name);
                impl MemoryMappedReaderWriter for $name {}
            };
        }

        impl_mmio_reader_writer!(u8);
        impl_mmio_reader_writer!(u16);
        impl_mmio_reader_writer!(u32);
        impl_mmio_reader_writer!(u64);
    }
}
