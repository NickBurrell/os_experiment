pub mod io {
    use core::marker::PhantomData;

    pub trait PortRead {
        unsafe fn read_port(port: u16) -> Self;
    }
    pub trait PortWrite {
        unsafe fn write_port(port: u16, value: Self);
    }

    pub trait PortReadWrite: PortRead + PortWrite {}

    impl PortRead for u8 {
        #[inline]
        unsafe fn read_port(port: u16) -> u8 {
            let value: u8;
            asm!("inb %dx, %al" : "={al}"(value) : "{dx}"(port) :: "volatile");
            value
        }
    }

    impl PortRead for u16 {
        #[inline]
        unsafe fn read_port(port: u16) -> u16 {
            let value: u16;
            asm!("inw %dx, %ax" : "={ax}"(value) : "{dx}"(port) :: "volatile");
            value
        }
    }

    impl PortRead for u32 {
        #[inline]
        unsafe fn read_port(port: u16) -> u32 {
            let value: u32;
            asm!("inl %dx, %eax" : "={eax}"(value) : "{dx}"(port) :: "volatile");
            value
        }
    }

    impl PortWrite for u8 {
        #[inline]
        unsafe fn write_port(port: u16, value: u8) {
            asm!("outb %al, %dx" :: "{dx}"(port), "{a}"(value) :: "volatile")
        }
    }

    impl PortWrite for u16 {
        #[inline]
        unsafe fn write_port(port: u16, value: u16) {
            asm!("outw %ax, %dx" :: "{dx}"(port), "{ax}"(value) :: "volatile")
        }
    }

    impl PortWrite for u32 {
        #[inline]
        unsafe fn write_port(port: u16, value: u32) {
            asm!("outl %eax, %dx" :: "{dx}"(port), "{eax}"(value) :: "volatile")
        }
    }

    impl PortReadWrite for u8 {}
    impl PortReadWrite for u16 {}
    impl PortReadWrite for u32 {}

    #[derive(Debug, Clone, Copy, PartialEq, Eq)]
    pub struct PortReadOnly<T: PortRead> {
        port: u16,
        phantom: PhantomData<T>,
    }

    #[derive(Debug, Clone, Copy, PartialEq, Eq)]
    pub struct PortWriteOnly<T: PortWrite> {
        port: u16,
        phantom: PhantomData<T>,
    }

    #[derive(Debug, Clone, Copy, PartialEq, Eq)]
    pub struct Port<T: PortReadWrite> {
        port: u16,
        phantom: PhantomData<T>,
    }

    impl<T: PortRead> PortReadOnly<T> {
        pub const fn new(port: u16) -> PortReadOnly<T> {
            PortReadOnly {
                port: port,
                phantom: PhantomData,
            }
        }

        #[inline]
        pub unsafe fn read(&mut self) -> T {
            T::read_port(self.port)
        }
    }

    impl<T: PortWrite> PortWriteOnly<T> {
        pub const fn new(port: u16) -> PortWriteOnly<T> {
            PortWriteOnly {
                port: port,
                phantom: PhantomData,
            }
        }
        #[inline]
        pub unsafe fn write(&mut self, value: T) {
            T::write_port(self.port, value)
        }
    }

    impl<T: PortReadWrite> Port<T> {
        pub const fn new(port: u16) -> Port<T> {
            Port {
                port: port,
                phantom: PhantomData,
            }
        }
        #[inline]
        pub unsafe fn read(&mut self) -> T {
            T::read_port(self.port)
        }
        #[inline]
        pub unsafe fn write(&mut self, value: T) {
            T::write_port(self.port, value)
        }
    }
}
