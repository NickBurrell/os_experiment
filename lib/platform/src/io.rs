pub trait IoReader {}
pub trait IoWriter {}
pub trait IoReaderWriter: IoReader + IoWriter {}

pub trait MemoryRegionReader<T> {
    unsafe fn read() -> T;
}
pub trait MemoryRegionWriter<T> {
    unsafe fn write(val: T);
}
pub trait MemoryRegionReaderWriter<T>: MemoryRegionReader<T> + MemoryRegionWriter<T> {}
