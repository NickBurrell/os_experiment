use bit_field::BitField;
use core::convert::{Into, TryInto};
use core::fmt;
use core::ops::{Add, AddAssign, Sub, SubAssign};
use ux::*;

#[cfg(target_arch = "aarch64")]
use crate::arch::aarch64::*;
#[cfg(target_arch = "arm")]
use crate::arch::arm::*;
#[cfg(target_arch = "x86")]
use crate::arch::x86::*;
#[cfg(target_arch = "x86")]
use crate::arch::x86_64::*;

#[cfg(target_pointer_width = "32")]
type Address = u32;
#[cfg(target_pointer_width = "64")]
type Address = u64;

pub trait Align {
    fn align_up<U>(self, align: U) -> Self
    where
        U: Into<Address>;
    fn align_down<U>(self, align: U) -> Self
    where
        U: Into<Address>;
    fn is_aligned<U>(self, align: U) -> bool
    where
        U: Into<Address>;
}

pub fn align_up(addr: Address, align: u64) -> u64 {
    assert!(align.is_power_of_two(), "`align` must be a power of two");
    let align_mask = align - 1;
    if addr & align_mask == 0 {
        addr
    } else {
        (addr | align_mask) + 1
    }
}

pub fn align_down(addr: Address, align: u64) -> u64 {
    assert!(align.is_power_of_two(), "`align` must be a power of two");
    addr & !(align - 1)
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[repr(transparent)]
pub struct VirtualAddress(pub(crate) Address);

#[derive(Debug)]
pub struct VirtualAddressNotValid(pub(crate) Address);

impl VirtualAddress {
    pub fn new(addr: Address) -> VirtualAddress {
        Self::try_new(addr).expect(
            "address passed to `VirtualAddress::new` must not contain any data \
             in bits 48 to 64",
        )
    }

    pub fn try_new(addr: Address) -> Result<VirtualAddress, VirtualAddressNotValid> {
        match addr.get_bits(47..64) {
            0 | 0x1FFFF => Ok(VirtualAddress(addr)),
            1 => Ok(VirtualAddress::new_unchecked(addr)),
            other => Err(VirtualAddressNotValid(other)),
        }
    }

    pub fn new_unchecked(mut addr: Address) -> VirtualAddress {
        if addr.get_bit(47) {
            addr.set_bits(48..64, 0xFFFF);
        } else {
            addr.set_bits(48..64, 0x0);
        }
        VirtualAddress(addr)
    }

    pub const fn zero() -> VirtualAddress {
        VirtualAddress(0)
    }

    pub fn as_ptr<T>(self) -> *const T {
        Address::from(self) as *const T
    }

    pub fn as_mut_ptr<T>(self) -> *mut T {
        Address::from(self) as *mut T
    }

    pub fn page_offset(&self) -> u12 {
        u12::new((self.0 & 0xFFF).try_into().unwrap())
    }

    pub fn from_ptr<T>(ptr: *const T) -> Self {
        Self::new(ptr as Address)
    }
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[repr(transparent)]
pub struct PhysicalAddress(pub(crate) Address);

#[derive(Debug)]
pub struct PhysicalAddressNotValid(pub(crate) Address);

impl PhysicalAddress {
    pub fn new(addr: Address) -> PhysicalAddress {
        assert_eq!(
            addr.get_bits(52..64),
            0,
            "physical addresses must not have any bits in the range 52 to 64 set"
        );
        PhysicalAddress(addr)
    }

    pub fn try_new(addr: Address) -> Result<PhysicalAddress, PhysicalAddressNotValid> {
        match addr.get_bits(52..64) {
            0 => Ok(PhysicalAddress(addr)),
            other => Err(PhysicalAddressNotValid(other)),
        }
    }

    pub fn is_null(&self) -> bool {
        self.0 == 0
    }
}

impl Align for VirtualAddress {
    fn align_up<U>(self, align: U) -> Self
    where
        U: Into<Address>,
    {
        VirtualAddress(align_up(self.0, align.into()))
    }

    fn align_down<U>(self, align: U) -> Self
    where
        U: Into<Address>,
    {
        VirtualAddress(align_down(self.0, align.into()))
    }

    fn is_aligned<U>(self, align: U) -> bool
    where
        U: Into<Address>,
    {
        self.align_down(align) == self
    }
}

impl From<VirtualAddress> for Address {
    fn from(addr: VirtualAddress) -> Address {
        addr.0
    }
}

impl fmt::Debug for VirtualAddress {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "VirtualAddress({:#x})", self.0)
    }
}

impl Add<Address> for VirtualAddress {
    type Output = Self;
    fn add(self, rhs: Address) -> Self::Output {
        VirtualAddress::new(self.0 + rhs)
    }
}

impl AddAssign<Address> for VirtualAddress {
    fn add_assign(&mut self, rhs: Address) {
        *self = *self + rhs;
    }
}

impl Add<usize> for VirtualAddress {
    type Output = Self;
    fn add(self, rhs: usize) -> Self::Output {
        self + rhs as u64
    }
}

impl AddAssign<usize> for VirtualAddress {
    fn add_assign(&mut self, rhs: usize) {
        self.add_assign(rhs as u64)
    }
}

impl Sub<Address> for VirtualAddress {
    type Output = Self;
    fn sub(self, rhs: Address) -> Self::Output {
        VirtualAddress::new(self.0.checked_sub(rhs).unwrap())
    }
}

impl SubAssign<Address> for VirtualAddress {
    fn sub_assign(&mut self, rhs: Address) {
        *self = *self - rhs;
    }
}

impl Sub<usize> for VirtualAddress {
    type Output = Self;
    fn sub(self, rhs: usize) -> Self::Output {
        self - rhs as u64
    }
}

impl SubAssign<usize> for VirtualAddress {
    fn sub_assign(&mut self, rhs: usize) {
        self.sub_assign(rhs as Address)
    }
}

impl Sub<VirtualAddress> for VirtualAddress {
    type Output = Address;
    fn sub(self, rhs: VirtualAddress) -> Self::Output {
        Address::from(self).checked_sub(Address::from(rhs)).unwrap()
    }
}

impl Align for PhysicalAddress {
    fn align_up<U>(self, align: U) -> Self
    where
        U: Into<Address>,
    {
        PhysicalAddress(align_up(self.0, align.into()))
    }

    fn align_down<U>(self, align: U) -> Self
    where
        U: Into<Address>,
    {
        PhysicalAddress(align_down(self.0, align.into()))
    }

    fn is_aligned<U>(self, align: U) -> bool
    where
        U: Into<Address>,
    {
        self.align_down(align) == self
    }
}

impl From<PhysicalAddress> for Address {
    fn from(addr: PhysicalAddress) -> Address {
        addr.0
    }
}

impl fmt::Debug for PhysicalAddress {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "PhysicalAddress({:#x})", self.0)
    }
}

impl fmt::Binary for PhysicalAddress {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl fmt::LowerHex for PhysicalAddress {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl fmt::Octal for PhysicalAddress {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl fmt::UpperHex for PhysicalAddress {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl Add<Address> for PhysicalAddress {
    type Output = Self;
    fn add(self, rhs: Address) -> Self::Output {
        PhysicalAddress::new(self.0 + rhs)
    }
}

impl AddAssign<Address> for PhysicalAddress {
    fn add_assign(&mut self, rhs: Address) {
        *self = *self + rhs;
    }
}

impl Add<usize> for PhysicalAddress {
    type Output = Self;
    fn add(self, rhs: usize) -> Self::Output {
        #[cfg(target_pointer_width = "32")]
        {
            self + cast::u32(rhs)
        }
        #[cfg(target_pointer_width = "64")]
        {
            self + cast::u64(rhs)
        }
    }
}

impl AddAssign<usize> for PhysicalAddress {
    fn add_assign(&mut self, rhs: usize) {
        #[cfg(target_pointer_width = "32")]
        {
            self.add_assign(cast::u32(rhs))
        }
        #[cfg(target_pointer_width = "64")]
        {
            self.add_assign(cast::u64(rhs))
        }
    }
}

impl Sub<Address> for PhysicalAddress {
    type Output = Self;
    fn sub(self, rhs: Address) -> Self::Output {
        PhysicalAddress::new(self.0.checked_sub(rhs).unwrap())
    }
}

impl SubAssign<Address> for PhysicalAddress {
    fn sub_assign(&mut self, rhs: Address) {
        *self = *self - rhs;
    }
}

impl Sub<usize> for PhysicalAddress {
    type Output = Self;
    fn sub(self, rhs: usize) -> Self::Output {
        #[cfg(target_pointer_width = "32")]
        {
            self - cast::u32(rhs)
        }
        #[cfg(target_pointer_width = "64")]
        {
            self - cast::u64(rhs)
        }
    }
}

impl SubAssign<usize> for PhysicalAddress {
    fn sub_assign(&mut self, rhs: usize) {
        #[cfg(target_pointer_width = "32")]
        {
            self.sub_assign(cast::u32(rhs))
        }
        #[cfg(target_pointer_width = "64")]
        {
            self.sub_assign(cast::u64(rhs))
        }
    }
}

impl Sub<PhysicalAddress> for PhysicalAddress {
    type Output = Address;
    fn sub(self, rhs: PhysicalAddress) -> Self::Output {
        Address::from(self).checked_sub(Address::from(rhs)).unwrap()
    }
}

#[cfg(target_pointer_width = "u32")]
impl core::convert::Into<u32> for PhysicalAddress {
    fn into(self) -> u32 {
        self.0
    }
}

#[cfg(target_pointer_width = "u64")]
impl core::convert::Into<u64> for PhysicalAddress {
    fn into(self) -> u64 {
        self.0
    }
}
