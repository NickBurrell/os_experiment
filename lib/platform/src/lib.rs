#![cfg_attr(not(test), no_std)]
#![feature(asm)]
#![feature(const_fn)]
#![cfg(any(target_arch = "x86", target_arch = "x86_64"))]
#![feature(abi_x86_interrupt)]

pub mod addr;
pub mod arch;
pub mod io;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
#[repr(u8)]
pub enum PrivilegeLevel {
    Ring0 = 0,
    Ring1 = 1,
    Ring2 = 2,
    Ring3 = 3,
}

impl From<u16> for PrivilegeLevel {
    fn from(value: u16) -> PrivilegeLevel {
        use PrivilegeLevel::*;
        match value {
            0 => Ring0,
            1 => Ring1,
            2 => Ring2,
            3 => Ring3,
            i => panic!("{} is not a valid privilege level", i),
        }
    }
}
