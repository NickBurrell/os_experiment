.section .boot, "awx"
.intel_syntax noprefix
.code32

stage_3:
    mov bx, 0x10
    mov ds, bx 
    mov es, bx 
    mov ss, bx 

    lea si, boot_third_stage_str
    call vga_println

check_cpu:
    call check_cpuid
    call check_long_mode

    cli                   

    lidt zero_idt         

set_up_page_tables:
    
    lea edi, [__page_table_start]
    lea ecx, [__page_table_end]
    sub ecx, edi
    shr ecx, 2 
    xor eax, eax
    rep stosd

    
    lea eax, [_p3]
    or eax, (1 | 2)
    mov [_p4], eax
    
    lea eax, [_p2]
    or eax, (1 | 2)
    mov [_p3], eax
    
    lea eax, [_p1]
    or eax, (1 | 2)
    mov [_p2], eax
    mov eax, (0x400000 | 1 | 2 | (1 << 7))
    mov ecx, 2
    lea edx, _kernel_size
    add edx, 0x400000 
    add edx, 0x200000 - 1 
    shr edx, 12 + 9 
    map_p2_table:
    mov [_p2 + ecx * 8], eax
    add eax, 0x200000
    add ecx, 1
    cmp ecx, edx
    jb map_p2_table
    
    lea eax, __page_table_start
    and eax, 0xfffff000
    or eax, (1 | 2)
    lea ecx, __page_table_start
    shr ecx, 12 
    lea edx, __bootloader_end
    add edx, 4096 - 1 
    shr edx, 12 
    map_p1_table:
    mov [_p1 + ecx * 8], eax
    add eax, 4096
    add ecx, 1
    cmp ecx, edx
    jb map_p1_table
    map_framebuffer:
    call vga_map_frame_buffer

enable_paging:
    
    
    wbinvd
    mfence

    
    lea eax, [_p4]
    mov cr3, eax

    
    mov eax, cr4
    or eax, (1 << 5)
    mov cr4, eax

    
    mov ecx, 0xC0000080
    rdmsr
    or eax, (1 << 8)
    wrmsr

    
    mov eax, cr0
    or eax, (1 << 31)
    mov cr0, eax

load_64bit_gdt:
    lgdt gdt_64_pointer                

jump_to_long_mode:
    push 0x8
    lea eax, [stage_4]
    push eax
    retf 

spin_here:
    jmp spin_here

check_cpuid:
    
    pushfd
    pop eax

    mov ecx, eax

    xor eax, (1 << 21)

    push eax
    popfd

    pushfd
    pop eax

    push ecx
    popfd
    
    cmp eax, ecx
    je no_cpuid
    ret
no_cpuid:
    lea esi, no_cpuid_str
    call vga_println
no_cpuid_spin:
    hlt
    jmp no_cpuid_spin

check_long_mode:
    mov eax, 0x80000000    
    cpuid                  
    cmp eax, 0x80000001    
    jb no_long_mode        

    mov eax, 0x80000001    
    cpuid                  
    test edx, (1 << 29)    
    jz no_long_mode        
    ret
no_long_mode:
    lea esi, no_long_mode_str
    call vga_println
no_long_mode_spin:
    hlt
    jmp no_long_mode_spin


.align 4
zero_idt:
    .word 0
    .byte 0

gdt_64:
    .quad 0x0000000000000000          
    .quad 0x00209A0000000000          
    .quad 0x0000920000000000          

.align 4
    .word 0                              

gdt_64_pointer:
    .word gdt_64_pointer - gdt_64 - 1    
    .long gdt_64                            

boot_third_stage_str: .asciz "Booting (third stage)..."
no_cpuid_str: .asciz "Error: CPU does not support CPUID"
no_long_mode_str: .asciz "Error: CPU does not support long mode"