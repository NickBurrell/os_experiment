.section .boot, "awx"
.intel_syntax noprefix
.code16

second_stage_start_str: .asciz "Booting (second stage)..."
kernel_load_failed_str: .asciz "Failed to load kernel from disk"

kernel_load_failed:
    lea si, [kernel_load_failed_str]
    call real_mode_println
kernel_load_failed_spin:
    jmp kernel_load_failed_spin

stage_2:
    lea si, [second_stage_start_str]
    call real_mode_println

set_target_operating_mode:
    
    pushf
    mov ax, 0xec00
    mov bl, 0x2
    int 0x15
    popf

load_kernel_from_disk:
    
    lea eax, _kernel_buffer
    mov [dap_buffer_addr], ax
    
    mov word ptr [dap_blocks], 1
    
    lea eax, _kernel_start_addr
    lea ebx, _start
    sub eax, ebx
    shr eax, 9 
    mov [dap_start_lba], eax
    
    mov edi, 0x400000
    
    lea ecx, _kernel_size
    add ecx, 511 
    shr ecx, 9

load_next_kernel_block_from_disk:
    
    lea si, dap
    mov ah, 0x42
    int 0x13
    jc kernel_load_failed
    
    push ecx
    push esi
    mov ecx, 512 / 4
    
    movzx esi, word ptr [dap_buffer_addr]
    
    rep movsd [edi], [esi]
    pop esi
    pop ecx
    
    mov eax, [dap_start_lba]
    add eax, 1
    mov [dap_start_lba], eax

    sub ecx, 1
    jnz load_next_kernel_block_from_disk

create_memory_map:
    lea di, es:[_memory_map]
    call do_e820

video_mode_config:
    call config_video_mode

enter_protected_mode_again:
    cli
    lgdt [gdt32info]
    mov eax, cr0
    or al, 1    
    mov cr0, eax

    push 0x8
    lea eax, [stage_3]
    push eax
    retf

spin32:
    jmp spin32
