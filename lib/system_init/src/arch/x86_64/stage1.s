.section .boot-first-stage, "awx"
.global _start
.intel_syntax noprefix
.code16

_start:
    xor ax, ax
    mov ds, ax
    mov es, ax
    mov ss, ax
    mov fs, ax
    mov gs, ax

    cld

    mov sp, 0x7c00

    lea si, boot_start_str
    call real_mode_println

enable_a20:
    in al, 0x92
    test al, 2
    jnz enable_a20_after
    or al, 2
    and al, 0xFE
    out 0x92, al

enable_a20_after:

enter_protected_mode:
    cli
    push ds
    push es

    lgdt [gdt32info]

    mov eax, cr0
    or al, 1
    mov cr0, eax
    jmp protected_mode

protected_mode:
    mov bx, 0x10
    mov ds, bx
    mov es, bx

    and al, 0xFE
    mov cr0, eax

unreal_mode:
    pop es
    pop ds
    sti

    mov bx, 0x0F01
    mov eax, 0xB8F00
    mov word ptr ds:[eax], bx

check_int32h_extensions:
    mov ah, 0x41
    mov bx, 0x55AA
    int 0x13
    jc no_int13h_extensions

load_rest_of_bootloader_from_disk:
    lea eax, _rest_of_bootloader_start_addr
    
    mov [dap_buffer_addr], ax
    
    lea ebx, _rest_of_bootloader_end_addr
    sub ebx, eax
    shr ebx, 9
    mov [dap_blocks], bx

    lea ebx, _start
    sub eax, ebx
    shr eax, 9
    mov [dap_start_lba], eax

    lea si, dap
    mov ah, 0x42
    int 0x13
    jc rest_of_bootloader_load_failed

jump_to_second_stage:
    lea eax, [stage_2]
    jmp eax

spin:
    jmp spin

real_mode_println:
    call read_mode_print
    mov al, 13
    call real_mode_print_char
    mov al, 10
    jmp real_mode_print_char

real_mode_print:
    cld
real_mode_print_loop:
    lodsb al, BYTE PTR [si]
    test al, al
    jz real_mode_print_done
    call real_mode_print_char
    jmp real_mode_print_loop

real_mode_print_char:
    mov ah, 0x0E
    int 0x10
    ret

real_mode_print_hex:
    mov cx, 4
.lp:
    mov al, bh
    shr al, 4

    cmp al, 0xA
    jb .below_0xA
    add al, 'A' - 0xA - '0'
.below_0xA:
    add al, '0'

    call real_mode_print_char

    shl bx, 4
    loop .lp

    ret

real_mode_error:
    call real_mode_println
    jmp spin

no_int13h_extensions:
    lea si, no_int13h_extensions_str
    jmp real_mode_error

rest_of_bootloader_load_failed:
    lea si, rest_of_bootloader_load_failed_str
    jmp real_mode_error

boot_start_str: .asciz "Booting (first stage)..."
error_str: .asciz "Error: "
no_int13h_extensions_str: .asciz "No support for int13h extensions"
rest_of_bootloader_load_failed_str: .asciz "Failed to load rest of bootloader"

gdt32info:
   .word gdt32_end - gdt32 - 1  # last byte in table
   .word gdt32                  # start of table

gdt32:
    # entry 0 is always unused
    .quad 0
codedesc:
    .byte 0xFF
    .byte 0xFF
    .byte 0
    .byte 0
    .byte 0
    .byte 0x9A
    .byte 0xCF
    .byte 0
datadesc:
    .byte 0xFF
    .byte 0xFF
    .byte 0
    .byte 0
    .byte 0
    .byte 0x92
    .byte 0xCF
    .byte 0
gdt32_end:

dap:
    .byte 0x10
    .byte 0
dap_blocks:
    .word 0
dap_buffer_addr:
    .word 0
dap_buffer_seg:
    .word 0
dap_start_lba:
    .quad 0

.org 510
.word 0xAA55