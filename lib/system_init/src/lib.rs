#![feature(lang_items)]
#![feature(global_asm)]
#![feature(step_trait)]
#![feature(asm)]
#![feature(nll)]
#![feature(const_fn)]
#![cfg_attr(not(test), no_std)]

#[cfg(not(target_os = "none"))]
compile_error!("Cannot compile `system_init` for target OS. Must compile for freestanding.");

pub mod arch;
