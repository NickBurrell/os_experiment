#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(os_experiment::test::test_runner)]
#![reexport_test_harness_main = "test_main"]
use os_experiment::{print, println, serial_print, serial_println};

#[test_case]
fn test_println() {
    serial_print!("test_println");
    println!("test_println output");
    serial_println!("[ok]");
}

#[no_mangle]
pub extern "C" fn _start() -> ! {
    test_main();
    loop {}
}

#[panic_handler]
fn panic(info: &core::panic::PanicInfo) -> ! {
    os_experiment::kernel::panic::test_panic_handler(info)
}
